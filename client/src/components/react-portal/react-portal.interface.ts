import React from "react";

export interface ReactPortalPropsInteraface {
  children: React.ReactNode;
  portalId: string;
}
