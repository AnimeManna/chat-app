import { FC } from "react";
import { createPortal } from "react-dom";
import { ReactPortalPropsInteraface } from "./react-portal.interface";

export const ReactPortal: FC<ReactPortalPropsInteraface> = ({
  children,
  portalId,
}) => {
  const createPortalElement = (id: string): HTMLDivElement => {
    const portalElement = document.createElement("div");
    portalElement.setAttribute("id", id);
    document.body.appendChild(portalElement);
    return portalElement;
  };

  let element = document.getElementById(portalId);

  if (!element) {
    element = createPortalElement(portalId);
  }

  return createPortal(children, element);
};
