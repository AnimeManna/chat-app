import { FC, useEffect } from "react";
import { ModalPropsInterface } from "./modal.interface";
import styles from "./modal.module.css";
import { ReactPortal } from "../react-portal";

export const Modal: FC<ModalPropsInterface> = ({ id, children, onClose }) => {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "unset";
    };
  }, []);

  useEffect(() => {
    const closeOnEscapeKey = (event: KeyboardEvent) =>
      event.key === "Escape" ? onClose() : null;
    document.body.addEventListener("keydown", closeOnEscapeKey);
    return () => {
      document.body.removeEventListener("keydown", closeOnEscapeKey);
    };
  }, [onClose]);

  return (
    <ReactPortal portalId={`${id}-modal`}>
      <div className={styles.container}>
        <div className={styles.blur} onClick={onClose} />
        <div className={styles.wrapper}>{children}</div>
      </div>
    </ReactPortal>
  );
};
