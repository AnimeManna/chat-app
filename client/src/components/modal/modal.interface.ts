import React from "react";

export interface ModalPropsInterface {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}
