import { UserType } from "../../app/user/user.type";

export interface SidebarPropsInterface {
  users: UserType[];
}
