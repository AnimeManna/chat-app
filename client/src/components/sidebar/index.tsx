import { FC } from "react";
import { SidebarPropsInterface } from "./sidebar.interface";
import styles from "./sidebar.module.css";

export const Sidebar: FC<SidebarPropsInterface> = ({ users }) => {
  return (
    <aside className={styles.container}>
      <ul className={styles.list}>
        {users.map((user) => (
          <li key={user.id} className={styles["list-item"]}>
            {user.username}
          </li>
        ))}
      </ul>
    </aside>
  );
};
