import { ChangeEvent, KeyboardEvent } from "react";

export interface TextFieldPropsInterface {
  name: string;
  label: string;
  placeholder?: string;
  type: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (event?: KeyboardEvent<HTMLInputElement>) => void;
  value: string;
  disabled?: boolean;
}
