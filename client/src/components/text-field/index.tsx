import { FC, useEffect, useMemo, useRef, useState } from "react";
import { mergeClassNames } from "../../helpers/functions/mergeClassNames";
import { TextFieldPropsInterface } from "./text-field.interface";
import styles from "./text-field.module.css";

export const TextField: FC<TextFieldPropsInterface> = ({
  label,
  disabled,
  ...inputProps
}) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [focused, setFocused] = useState<boolean>(false);
  console.log(inputProps);
  const handleClickOutside: EventListener = (event) => {
    if (inputRef.current && !inputRef.current.contains(event.target as Node)) {
      setFocused(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const filled = useMemo(
    () => !!inputRef.current?.value || !!inputProps.value,
    [inputRef.current?.value, inputProps.value]
  );

  return (
    <div
      className={mergeClassNames(styles.container, {
        [styles.filled]: filled || focused,
        [styles.disabled]: disabled,
      })}
      onClick={() => {
        setFocused(true);
        inputRef.current?.focus();
      }}
    >
      <label className={styles.label}>{label}</label>
      <input
        {...inputProps}
        disabled={disabled}
        className={styles.input}
        ref={inputRef}
      />
    </div>
  );
};
