import { FC, useEffect } from "react";
import styles from "./header.module.css";
import { Login } from "../../containers/login";
import { useModal } from "../../helpers/hooks/useModal";
import { useUserContext } from "../../app/user/context";
import { socket } from "../../app/socket";
import { UserType } from "../../app/user/user.type";

export const Header: FC = () => {
  const [openLoginModal, LoginModal, closeLoginModal] = useModal("login");
  const { user, authorizeUser, logOut } = useUserContext();

  const authUserSocket = (user: UserType) => {
    socket.emit("newUser", user);
  };

  useEffect(() => {
    if (!user) {
      openLoginModal();
    } else {
      authUserSocket(user);
      closeLoginModal();
    }
  }, [user]);

  const handleLoginSubmit = (username: string) => {
    const userObject = { username, id: socket.id };
    authorizeUser(userObject);
  };

  const handleLogout = () => {
    socket.emit("logout", user?.id);
    logOut();
  };

  return (
    <header className={styles.container}>
      <p className={styles.logo}>Chat app</p>
      {user ? (
        <div>
          <p>{user.username}</p>
          <button className={styles["login-button"]} onClick={handleLogout}>
            Log out
          </button>
        </div>
      ) : (
        <button className={styles["login-button"]} onClick={openLoginModal}>
          Login
        </button>
      )}
      <LoginModal>
        <Login onSubmit={handleLoginSubmit} />
      </LoginModal>
    </header>
  );
};
