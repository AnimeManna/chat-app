import { UserType } from "./user.type";

export interface UserContextInterface {
  user: UserType | null;
  authorizeUser: (user: UserType) => void;
  logOut: () => void;
}
