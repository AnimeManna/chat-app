import React, { FC, useEffect, useState } from "react";
import { UserContext } from "./context";
import { UserType } from "./user.type";

export const UserProvider: FC<{
  children: React.ReactNode;
}> = ({ children }) => {
  const [user, setUser] = useState<UserType | null>(null);

  useEffect(() => {
    try {
      const localUser = window.localStorage.getItem("user");
      if (localUser) {
        const user = JSON.parse(localUser);
        setUser(user);
      }
    } catch (e) {
      console.log("Sorry can't parse user");
    }
  }, []);

  const authorizeUser = (user: UserType) => {
    try {
      window.localStorage.setItem("user", JSON.stringify(user));
      setUser(user);
    } catch (e) {
      console.log("Sorry can't put user in local storage. Try again later");
    }
  };

  const logOut = () => {
    window.localStorage.removeItem("user");
    setUser(null);
  };

  return (
    <UserContext.Provider value={{ user, authorizeUser, logOut }}>
      {children}
    </UserContext.Provider>
  );
};
