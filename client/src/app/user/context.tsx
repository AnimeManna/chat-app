import { createContext, useContext } from "react";
import { UserContextInterface } from "./context.interface";

export const UserContext = createContext({} as UserContextInterface);

export const useUserContext = () => useContext(UserContext);
