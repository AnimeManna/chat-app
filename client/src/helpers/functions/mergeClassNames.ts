export function mergeClassNames(
  ...args: Array<
    string | number | Record<string, boolean | null | void> | undefined
  >
): string {
  const classes = [];

  for (const arg of args) {
    const argType = typeof arg;

    if (argType === "object") {
      const obj = arg as Record<string, boolean>;

      Object.keys(obj).forEach((className) => {
        const value = obj[className];

        if (value) {
          classes.push(className);
        }
      });
    } else if (argType === "string") {
      classes.push((arg as string).trim());
    } else if (argType === "number") {
      classes.push(arg);
    }
  }

  return classes.join(" ");
}
