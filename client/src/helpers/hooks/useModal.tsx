import { Modal } from "../../components/modal";
import React, { FC, useState, useMemo, PropsWithChildren } from "react";

interface ModalComponentProps extends PropsWithChildren {}

export const useModal = (
  id: string
): [
  openModal: () => void,
  ModalComponent: FC<ModalComponentProps>,
  closeModal: () => void
] => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const openModal = () => setIsOpen(true);

  const closeModal = () => setIsOpen(false);

  const ModalComponent: FC<ModalComponentProps> = useMemo(() => {
    return (props) => {
      if (!isOpen) return null;

      return (
        <Modal id={id} onClose={closeModal}>
          {props.children}
        </Modal>
      );
    };
  }, [id, isOpen]);

  return [openModal, ModalComponent, closeModal];
};
