import { ChangeEvent, FC, FormEvent, useEffect, useRef, useState } from "react";
import styles from "./layout.module.css";
import { TextField } from "../../components/text-field";
import { socket } from "../../app/socket";
import { Sidebar } from "../../components/sidebar";
import { UserType } from "../../app/user/user.type";
import { useUserContext } from "../../app/user/context";
import { MessageType } from "../messages-list/message.type";
import { MessagesList } from "../messages-list";

export const Layout: FC = () => {
  const [message, setMessage] = useState<string>("");
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [typing, setTyping] = useState<boolean>(false);
  const [typingStatus, setTypingStatus] = useState<string | null>(null);
  const [users, setUsers] = useState<UserType[]>([]);
  const { user } = useUserContext();
  const lastMessageRef = useRef<HTMLLIElement>(null);

  const handleChangeMessage = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setMessage(value);
  };

  useEffect(() => {
    socket.on("newUserResponse", (data) =>
      setUsers(
        data.map((item: { username: string; socketID: string }) => ({
          username: item.username,
          id: item.socketID,
        }))
      )
    );
  }, [socket, users]);

  useEffect(() => {
    socket.on("messageResponse", (data) => setMessages([...messages, data]));
  }, [socket, messages]);

  useEffect(() => {
    lastMessageRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  useEffect(() => {
    socket.on("typingResponse", (data) => setTypingStatus(data));
  }, [socket]);

  const handleSubmitMessage = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (message.trim() && user) {
      socket.emit("message", {
        text: message,
        name: user.username,
        id: `${socket.id}${Math.random()}`,
        socketID: user.id,
      });
    }
    setMessage("");
  };

  const timeoutFunction = () => {
    setTyping(false);
    socket.emit("noLongerTyping", user);
  };

  const handleTypingMessage = () => {
    let timer;
    if (!typing) {
      setTyping(true);
      socket.emit("typing", `${user?.username} is typing...`);
      timer = setTimeout(timeoutFunction, 5000);
    } else {
      clearTimeout(timer);
      timer = setTimeout(timeoutFunction, 5000);
    }
  };

  return (
    <article className={styles.container}>
      <Sidebar users={users} />
      <section className={styles["message-section"]}>
        <MessagesList messages={messages} lastMessageRef={lastMessageRef} />
        {typingStatus && <p>{typingStatus}</p>}
        <form
          onSubmit={handleSubmitMessage}
          className={styles["send-message__form"]}
        >
          <TextField
            name="Message"
            label="Your message"
            type="text"
            value={message}
            onChange={handleChangeMessage}
            disabled={!user}
            onKeyDown={handleTypingMessage}
          />
          <button
            className={styles["send-message__form__button"]}
            type="submit"
            disabled={!user}
          >
            Send
          </button>
        </form>
      </section>
    </article>
  );
};
