import React, { FC } from "react";
import styles from "./app.module.css";
import { Layout } from "../layout";
import { Header } from "../../components/header";

export const App: FC = () => {
  return (
    <main className={styles.container}>
      <Header />
      <Layout />
    </main>
  );
};
