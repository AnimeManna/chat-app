import { FC } from "react";
import { MessagesListPropsInterface } from "./messages-list.interface";
import styles from "./messages-list.module.css";

export const MessagesList: FC<MessagesListPropsInterface> = ({
  messages,
  lastMessageRef,
}) => {
  return (
    <ul className={styles.container}>
      {messages.map((message) => (
        <li key={message.id} className={styles.message}>
          <p className={styles["message-author"]}>{message.name}</p>
          <p className={styles["message-text"]}>{message.text}</p>
        </li>
      ))}
      <li ref={lastMessageRef} />
    </ul>
  );
};
