import { RefObject } from "react";
import { MessageType } from "./message.type";

export interface MessagesListPropsInterface {
  messages: MessageType[];
  lastMessageRef: RefObject<HTMLLIElement>;
}
