export type MessageType = {
  text: string;
  name: string;
  id: string;
  socketID: string;
};
