import { ChangeEvent, FC, FormEvent, useState } from "react";
import { TextField } from "../../components/text-field";
import styles from "./login.module.css";
import { LoginPropsInterface } from "./login.interface";

export const Login: FC<LoginPropsInterface> = ({ onSubmit }) => {
  const [value, setValue] = useState<string>("");

  const handleLoginChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!!value) {
      onSubmit(value);
    }
  };

  return (
    <form className={styles.container} onSubmit={handleSubmit}>
      <h2>Could you please enter your name for chat?</h2>
      <TextField
        name="login"
        label="Your login"
        type="text"
        value={value}
        onChange={handleLoginChange}
      />
    </form>
  );
};
