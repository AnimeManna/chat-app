export interface LoginPropsInterface {
  onSubmit: (username: string) => void;
}
