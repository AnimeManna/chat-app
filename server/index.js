const express = require("express");
const cors = require("cors");
const app = express();
const http = require("http").Server(app);
const PORT = 4000;

app.use(cors());

const socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:3000",
  },
});

let users = [];

socketIO.on("connection", (socket) => {
  console.log(` ${socket.id} user just connected!`);
  socket.on("newUser", (data) => {
    users.push({
      ...data,
      socketID: data.socketID ? data.socketID : socket.id,
    });
    socketIO.emit("newUserResponse", users);
  });

  socket.on("message", (data) => {
    socketIO.emit("messageResponse", data);
  });

  socket.on("logout", (data) => {
    console.log(`User by ${data} id has logout`);
    users = users.filter((user) => user.socketID !== data);
    socketIO.emit("newUserResponse", users);
  });

  socket.on("typing", (data) => {
    console.log(data);
    socket.broadcast.emit("typingResponse", data);
  });

  socket.on("noLongerTyping", () => {
    socket.broadcast.emit("typingResponse", "");
  });

  socket.on("disconnect", () => {
    console.log(" A user disconnected");
    users = users.filter((user) => user.socketID !== socket.id);
    socketIO.emit("newUserResponse", users);
    socket.disconnect();
  });
});

app.get("/api", (req, res) => {
  res.json({
    message: "Hello world",
  });
});

http.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
